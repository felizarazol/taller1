package model;

public class File {
	public String fileType;
	public Byte content [];
	public double size;
	
	public File (){
		fileType = "";
		content = new Byte[30];
		size = 0;
	}
	
	public String getFileType (){
		return fileType;
	}
	
	public void setFileType(String fileType){
		this.fileType = fileType;
	}
	
	public Byte [] getContent (){
		return content;
	}
	
	public void setContent (Byte[] content){
		this.content = content;
	}
	
	public double getSize (){
		return size;
	}
	
	public void setSize (double size){
		this.size = size;
	}
	
	public void download (){}
	
	public void share (){}
}