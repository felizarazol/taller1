/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tallersft_2;

import java.util.ArrayList;

/**
 *
 * @author Camilo
 */
public class Regular extends User {
    private int postViews;
    private int strikesNumber;
    private int starsNumber;
    private ArrayList posts = new ArrayList<Post>();
  
    public Regular(){
    }

    public void setPost(Post post) {
        this.posts.add(post);
    }

    public int getPostViews() {
        return postViews;
    }

    public void setPostViews(int postViews) {
        this.postViews = postViews;
    }

    public int getStrikesNumber() {
        return strikesNumber;
    }

    public void setStrikesNumber(int strikesNumber) {
        this.strikesNumber = strikesNumber;
    }

    public int getStarsNumber() {
        return starsNumber;
    }

    public ArrayList getPosts() {
        return posts;
    }
    
    public void setStarsNumber(int starsNumber) {
        this.starsNumber = starsNumber;
    }
}
