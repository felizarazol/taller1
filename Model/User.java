/*
 *  Gerardo Torres
 */

package model

class User{
	String name;
	String lastname;
	int age;
	String username;
	String password
	
	public User(String theName, StrictMath theLastname, int theAge, String theUsername, String thePassword){
		this.name = theName;
		this.lastname = theLastname;
		this.age = theAge;
		this.username = theUsername;
		this.password = thePassword;
	}
	
	public String getName()
    {
		return name;
    }
    
	public void setName(String theName)
    {
		this.name = theName;
    }
	
		public String getLastame()
    {
		return lastname;
    }
    
	public void setLastname(String theLastname)
    {
		this.lastname = theLastname;
    }
		
	public int getAge()
    {
		return age;
    }
    
	public void setAge(String theAge)
    {
		this.age = theAge;
    }
	
	public String getUsername()
    {
		return username;
    }
    
	public void setUsername(String theUsername)
    {
		this.username = theUsername;
    }
	
	
	public String getPassword()
    {
		return password;
    }
    
	public void setName(String thePassword)
    {
		this.password = thePassword;
    }
	
}